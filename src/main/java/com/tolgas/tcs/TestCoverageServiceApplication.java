package com.tolgas.tcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCoverageServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCoverageServiceApplication.class, args);
    }

}
