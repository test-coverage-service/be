package com.tolgas.tcs.base.api;

import com.tolgas.tcs.base.model.document.InformationActionDocument;
import com.tolgas.tcs.base.model.document.InformationProcessDocument;
import com.tolgas.tcs.base.repository.InformationProcessRepository;
import com.tolgas.tcs.rest.model.document.RestInformationAction;
import com.tolgas.tcs.rest.model.document.RestRequest;
import com.tolgas.tcs.rest.model.document.RestResponse;
import com.tolgas.tcs.rest.repository.RestInformationActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.tolgas.tcs.base.model.document.InformationActionDocument.ActionType.REST;

@RestController
public class InformationProcessController {

    @Autowired
    InformationProcessRepository informationProcessRepository;

    @Autowired
    RestInformationActionRepository restInformationActionRepository;

    @PostMapping("/api/v1/information-process")
    public InformationProcessDocument create(@RequestBody InformationProcessDocument informationProcessDocument) {
        InformationProcessDocument informationProcessDocument1 = new InformationProcessDocument();
        informationProcessDocument1.setId(1L);
        informationProcessDocument1.setActions(
                List.of(
                        new InformationActionDocument(1L, 1L, REST, InformationActionDocument.ProcessResult.SUCCESS),
                        new InformationActionDocument(2L, 2L, REST, InformationActionDocument.ProcessResult.SUCCESS),
                        new InformationActionDocument(3L, 3L, REST, InformationActionDocument.ProcessResult.FAILED)
                )
        );

        informationProcessRepository.save(informationProcessDocument1);

        restInformationActionRepository.save(
                new RestInformationAction(
                        1L,
                        new RestRequest("/api/v1/user", "{\"user\":\"name\": \"petya\"}", RestRequest.HttpType.POST),
                        new RestResponse(200, "OK")
                )
        );

        restInformationActionRepository.save(
                new RestInformationAction(
                        1L,
                        new RestRequest("/api/v1/user/petya", "{\"user\":\"name\": \"petya\"}", RestRequest.HttpType.PUT),
                        new RestResponse(200, "OK")
                )
        );

        restInformationActionRepository.save(
                new RestInformationAction(
                        1L,
                        new RestRequest("/api/v1/user/petya", "{\"user\":\"name\": \"petya\"}", RestRequest.HttpType.DELETE),
                        new RestResponse(204, "No body")
                )
        );

        return null;
    }

    @GetMapping("/api/v1/information-process/{id}")
    public InformationProcessDocument create(@PathVariable Long id) {
        return null;
    }

    @DeleteMapping("/api/v1/information-process/{id}")
    public void delete(@PathVariable Long id) {
    }

    @PutMapping("/api/v1/information-process/{id}")
    public InformationProcessDocument create(@PathVariable Long id,
                                             @RequestBody InformationProcessDocument informationProcessDocument) {
        return null;
    }
}
