package com.tolgas.tcs.base.repository;

import com.tolgas.tcs.base.model.document.InformationProcessDocument;
import org.springframework.data.repository.Repository;

public interface InformationProcessRepository extends Repository<InformationProcessDocument, Integer> {
    void save(InformationProcessDocument informationProcessDocument);
}
