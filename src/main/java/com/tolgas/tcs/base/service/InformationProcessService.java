package com.tolgas.tcs.base.service;

import com.tolgas.tcs.base.model.document.InformationActionDocument;
import com.tolgas.tcs.base.model.document.InformationProcessDocument;
import com.tolgas.tcs.base.repository.InformationProcessRepository;
import com.tolgas.tcs.graphql.service.GraphqlInformationActionProcessor;
import com.tolgas.tcs.rest.service.RestInformationActionProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class InformationProcessService {
    private Logger logger;

    @Autowired
    private InformationProcessRepository informationProcessRepository;
    @Autowired
    private RestInformationActionProcessor restInformationActionProcessor;
    @Autowired
    private GraphqlInformationActionProcessor graphqlInformationActionProcessor;

    public void process(InformationProcessDocument informationProcess) {
        logger.log(Level.INFO, "Information process starting... ");

        List<? extends InformationActionDocument> actions = informationProcess.getActions();

        if (CollectionUtils.isEmpty(actions)) {
            throw new IllegalArgumentException("Test actions cannot be empty");
        }

/*        for (InformationActionDocument testAction : actions) {
            switch (testAction) {
                case RestInformationAction restInformationAction -> {
                    logger.log(Level.INFO, "Rest Information Action recognized, process delegated... ");
                    restInformationActionProcessor.process(restInformationAction);
                }
                case GraphqlInformationAction graphqlInformationAction -> {
                    logger.log(Level.INFO, "Graphql Information Action recognized, process delegated... ");
                    graphqlInformationActionProcessor.process(graphqlInformationAction);
                }
                default -> throw new IllegalStateException("Unexpected action type: " + testAction);
            }
        }*/
    }
}
