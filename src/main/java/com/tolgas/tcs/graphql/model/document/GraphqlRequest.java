package com.tolgas.tcs.graphql.model.document;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

public class GraphqlRequest {
    private String query;
    private List<String> variables;

    public GraphqlRequest(String query, List<String> variables) {
        this.query = query;
        this.variables = variables;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }
}
