package com.tolgas.tcs.graphql.repository;

import com.tolgas.tcs.graphql.model.document.GraphqlInformationAction;
import org.springframework.data.repository.Repository;

public interface GraphqlInformationActionRepository  extends Repository<GraphqlInformationAction, Long> {
    GraphqlInformationAction save(GraphqlInformationAction restInformationAction);
}
