package com.tolgas.tcs.graphql.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tolgas.tcs.base.service.InformationActionProcessor;
import com.tolgas.tcs.graphql.model.document.GraphqlInformationAction;
import com.tolgas.tcs.graphql.model.document.GraphqlRequest;
import com.tolgas.tcs.graphql.repository.GraphqlInformationActionRepository;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.http.HttpClient;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class GraphqlInformationActionProcessor implements InformationActionProcessor<GraphqlInformationAction> {
    @Autowired
    private GraphqlInformationActionRepository restInformationActionRepository;
    private RestTemplate restTemplate;

    private Logger logger;
    @Override
    public void process(GraphqlInformationAction action) {
        logger.log(Level.INFO, "Starting process for GraphQl information action...");

        GraphqlRequest request = action.getGraphqlRequest();

        HttpClient client = HttpClient.newBuilder().build();
        GraphQLWebClient graphQLWebClient = GraphQLWebClient.newInstance(WebClient.builder().build(), new ObjectMapper());
        Mono<Object> result = graphQLWebClient.post(request.getQuery(), Object.class);
        action.getGraphqlResponse().setBody(result.block());

        restInformationActionRepository.save(action);
    }
}
