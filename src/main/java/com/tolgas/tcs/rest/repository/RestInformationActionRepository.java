package com.tolgas.tcs.rest.repository;

import com.tolgas.tcs.rest.model.document.RestInformationAction;
import org.springframework.data.repository.Repository;

public interface RestInformationActionRepository extends Repository<RestInformationAction, Long> {
    RestInformationAction save(RestInformationAction restInformationAction);
}
