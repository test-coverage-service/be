package com.tolgas.tcs.rest.service;

import com.tolgas.tcs.base.service.InformationActionProcessor;
import com.tolgas.tcs.rest.model.document.RestInformationAction;
import com.tolgas.tcs.rest.model.document.RestRequest;
import com.tolgas.tcs.rest.model.document.RestResponse;
import com.tolgas.tcs.rest.repository.RestInformationActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class RestInformationActionProcessor implements InformationActionProcessor<RestInformationAction> {

    @Autowired
    private RestInformationActionRepository restInformationActionRepository;
    private RestTemplate restTemplate;

    private Logger logger;

    @Override
    public void process(RestInformationAction action) {
        logger.log(Level.INFO, "Starting process for Rest information action...");

        RestRequest testRequest = action.getRequest();

        RequestEntity<?> request = testRequest.toRequestEntity();

        ResponseEntity<Object> actualResponseEntity = restTemplate.exchange(request, Object.class);

        RestResponse expectedResponse = action.getResponse();

        boolean isExpectedAndActualEquals =
                expectedResponse.getCode() == actualResponseEntity.getStatusCode().value()
                        && expectedResponse.getBody().equals(actualResponseEntity.getBody());

        restInformationActionRepository.save(action);
    }
}
